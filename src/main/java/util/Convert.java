package util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import bean.Cartelera;
import bean.Complejo;
import bean.Pelicula;
import bean.Sala;

public class Convert {

	public static String toMD5(String value){

	    MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	    md.update(value.getBytes());

	    byte byteData[] = md.digest();

	    StringBuffer sb = new StringBuffer();
	    for (int i = 0; i < byteData.length; i++)
	        sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));

	    return sb.toString();
	}
	
	public static Pelicula obtenerPelicula(ResultSet sr, Connection conexion){
		Pelicula p = null;
		try {
			p = new Pelicula();
			p.setIdPelicula(sr.getInt("idPelicula"));
			p.setNombre(sr.getString("nombre"));
			p.setImagen(sr.getString("imagen"));
			p.setFechaEstreno(sr.getString("fechaEstreno"));
			p.setPais(sr.getString("pais"));
			p.setDirector(sr.getString("director"));
			p.setActores(sr.getString("actores"));
			p.setIdioma(sr.getString("idioma"));
			p.setDuracion(sr.getInt("duracion"));
			p.setEdadRestriccion(sr.getInt("añoRestriccion"));
			p.setSinopsis(sr.getString("sinopsis"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return p;
	}
	
	public static Cartelera obtenerCartelera(ResultSet rs, Connection conexion){
		Cartelera c = null;
		try {
			c = new Cartelera();
			c.setIdCartelera(rs.getInt("idCartelera"));
			c.setFecha(rs.getString("fecha"));
			c.setHoraInicio(rs.getString("horaInicio"));
			/************************************/
			Pelicula peliculaBean = null;
			ResultSet resultPelicula = null;
			try {
				resultPelicula = conexion.prepareStatement("select * from Pelicula where idPelicula = " + rs.getInt("idPelicula")).executeQuery();
				if(resultPelicula.next()){
					peliculaBean = obtenerPelicula(resultPelicula, conexion);
				}	
				resultPelicula.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			c.setPelicula(peliculaBean);
			/************************************/
			Sala s = null;
			c.setIdSala(rs.getInt("idSala"));
			ResultSet resultSala = conexion.prepareStatement("select * from Sala where idSala = " + c.getIdSala()).executeQuery();
			if(resultSala.next()){
				s = new Sala();
				s.setIdSala(resultSala.getInt("idSala"));
				s.setNumero(resultSala.getInt("numero"));
				/******************************************/
				Complejo complejoBean = null;
				ResultSet resultComplejo = null;
				try {
					resultComplejo = conexion.prepareStatement("select * from Complejo where idComplejo = " + resultSala.getInt("idComplejo")).executeQuery();
					if(resultComplejo.next()){
						complejoBean = new Complejo();
						complejoBean.setIdComplejo(resultComplejo.getInt("idComplejo"));
						complejoBean.setNombre(resultComplejo.getString("nombre"));
					}	
				} catch (Exception e) {
					e.printStackTrace();
				}
				s.setComplejo(complejoBean);
				resultComplejo.close();
				/******************************************/
			}
			c.setSala(s);
			resultSala.close();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
				
		return c;
	}
	
}
