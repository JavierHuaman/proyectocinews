package bean;

import java.io.Serializable;


/**
 * The persistent class for the reservabutaca database table.
 * 
 */
public class Reservabutaca implements Serializable {
	private static final long serialVersionUID = 1L;

	private int idReservaButaca;

	private String butaca;
	
	private double precio;
	
	private Reserva reserva;
	
	private int idSala;

	public Reservabutaca() {
		reserva = new Reserva();
	}

	public int getIdReservaButaca() {
		return this.idReservaButaca;
	}

	public void setIdReservaButaca(int idReservaButaca) {
		this.idReservaButaca = idReservaButaca;
	}

	public String getButaca() {
		return this.butaca;
	}

	public void setButaca(String butaca) {
		this.butaca = butaca;
	}

	public Reserva getReserva() {
		return this.reserva;
	}

	public void setReserva(Reserva reserva) {
		this.reserva = reserva;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public int getIdSala() {
		return idSala;
	}

	public void setIdSala(int idSala) {
		this.idSala = idSala;
	}
	
}