package bean;

import java.io.Serializable;


/**
 * The persistent class for the sala database table.
 * 
 */
public class Sala implements Serializable {
	private static final long serialVersionUID = 1L;

	private int idSala;

	private int numero;

	private Cartelera[] carteleras;

	private Complejo complejo;

	public Sala() {
		complejo = new Complejo();
	}

	public int getIdSala() {
		return this.idSala;
	}

	public void setIdSala(int idSala) {
		this.idSala = idSala;
	}

	public int getNumero() {
		return this.numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public Cartelera[] getCarteleras() {
		return this.carteleras;
	}

	public void setCarteleras(Cartelera[] carteleras) {
		this.carteleras = carteleras;
	}
	
	public Complejo getComplejo() {
		return this.complejo;
	}

	public void setComplejo(Complejo complejo) {
		this.complejo = complejo;
	}

}