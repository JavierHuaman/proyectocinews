package bean;

import java.io.Serializable;


/**
 * The persistent class for the categoria database table.
 * 
 */
public class Categoria implements Serializable {
	private static final long serialVersionUID = 1L;

	private int idCategoria;

	private String descripcion;

	private Pelicula[] peliculas;

	public Categoria() {
	}

	public int getIdCategoria() {
		return this.idCategoria;
	}

	public void setIdCategoria(int idCategoria) {
		this.idCategoria = idCategoria;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Pelicula[] getPeliculas() {
		return this.peliculas;
	}

	public void setPeliculas(Pelicula[] peliculas) {
		this.peliculas = peliculas;
	}
}