package bean;

import java.io.Serializable;


/**
 * The persistent class for the cartelera database table.
 * 
 */
public class Cartelera implements Serializable {
	private static final long serialVersionUID = 1L;

	private int idCartelera;

	private String fecha;

	private String horaInicio;

	private String precio;

	private Pelicula pelicula;

	private Sala sala;
	
	private int idSala;

	public Cartelera() {
	}

	public int getIdCartelera() {
		return this.idCartelera;
	}

	public void setIdCartelera(int idCartelera) {
		this.idCartelera = idCartelera;
	}

	public String getFecha() {
		return this.fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getHoraInicio() {
		return this.horaInicio;
	}

	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}

	public String getPrecio() {
		return this.precio;
	}

	public void setPrecio(String precio) {
		this.precio = precio;
	}

	public Pelicula getPelicula() {
		return this.pelicula;
	}

	public void setPelicula(Pelicula pelicula) {
		this.pelicula = pelicula;
	}

	public Sala getSala() {
		return this.sala;
	}

	public void setSala(Sala sala) {
		this.sala = sala;
	}

	public int getIdSala() {
		return idSala;
	}

	public void setIdSala(int idSala) {
		this.idSala = idSala;
	}
}