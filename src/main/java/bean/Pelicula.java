package bean;

import java.io.Serializable;


/**
 * The persistent class for the pelicula database table.
 * 
 */
public class Pelicula implements Serializable {
	private static final long serialVersionUID = 1L;

	private int idPelicula;

	private String actores;

	private int edadRestriccion;

	private String director;

	private int duracion;

	private String fechaEstreno;

	private String idioma;

	private String imagen;

	private String nombre;

	private String pais;

	private String sinopsis;

	private Cartelera[] carteleras;

	private Categoria categoria;

	public Pelicula() {
	}

	public int getIdPelicula() {
		return this.idPelicula;
	}

	public void setIdPelicula(int idPelicula) {
		this.idPelicula = idPelicula;
	}

	public String getActores() {
		return this.actores;
	}

	public void setActores(String actores) {
		this.actores = actores;
	}

	public int getEdadRestriccion() {
		return this.edadRestriccion;
	}

	public void setEdadRestriccion(int edadRestriccion) {
		this.edadRestriccion = edadRestriccion;
	}

	public String getDirector() {
		return this.director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public int getDuracion() {
		return this.duracion;
	}

	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}

	public String getFechaEstreno() {
		return this.fechaEstreno;
	}

	public void setFechaEstreno(String fechaEstreno) {
		this.fechaEstreno = fechaEstreno;
	}

	public String getIdioma() {
		return this.idioma;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	public String getImagen() {
		return this.imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPais() {
		return this.pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getSinopsis() {
		return this.sinopsis;
	}

	public void setSinopsis(String sinopsis) {
		this.sinopsis = sinopsis;
	}

	public Cartelera[] getCarteleras() {
		return this.carteleras;
	}

	public void setCarteleras(Cartelera[] carteleras) {
		this.carteleras = carteleras;
	}
	
	public Categoria getCategoria() {
		return this.categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

}