package bean;

import java.io.Serializable;


/**
 * The persistent class for the reserva database table.
 * 
 */
public class Reserva implements Serializable {
	private static final long serialVersionUID = 1L;

	private int idReserva;

	private String estado;

	private String fechaReserva;

	private Cliente cliente;
	
	private int idCliente;
	
	private Cartelera cartelera;
	
	private int idCartelera;
	
	private Reservabutaca[] reservabutacas;

	private String numeroTicket;
	
	public Reserva() {
	}

	public int getIdReserva() {
		return this.idReserva;
	}

	public void setIdReserva(int idReserva) {
		this.idReserva = idReserva;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getFechaReserva() {
		return this.fechaReserva;
	}

	public void setFechaReserva(String fechaReserva) {
		this.fechaReserva = fechaReserva;
	}

	public Cliente getCliente() {
		return this.cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Cartelera getCartelera() {
		return this.cartelera;
	}

	public void setCartelera(Cartelera cartelera) {
		this.cartelera = cartelera;
	}

	public Reservabutaca[] getReservabutacas() {
		return this.reservabutacas;
	}

	public void setReservabutacas(Reservabutaca[] reservabutacas) {
		this.reservabutacas = reservabutacas;
	}

	public int getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}

	public int getIdCartelera() {
		return idCartelera;
	}

	public void setIdCartelera(int idCartelera) {
		this.idCartelera = idCartelera;
	}

	public String getNumeroTicket() {
		return numeroTicket;
	}

	public void setNumeroTicket(String numeroTicket) {
		this.numeroTicket = numeroTicket;
	}
	
	
}