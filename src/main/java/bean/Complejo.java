package bean;

import java.io.Serializable;


/**
 * The persistent class for the complejo database table.
 * 
 */
public class Complejo implements Serializable {
	private static final long serialVersionUID = 1L;

	private int idComplejo;

	private String nombre;

	private Ciudad ciudad;

	private Sala[] salas;

	public Complejo() {
	}

	public int getIdComplejo() {
		return this.idComplejo;
	}

	public void setIdComplejo(int idComplejo) {
		this.idComplejo = idComplejo;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Ciudad getCiudad() {
		return this.ciudad;
	}

	public void setCiudad(Ciudad ciudad) {
		this.ciudad = ciudad;
	}

	public Sala[] getSalas() {
		return this.salas;
	}

	public void setSalas(Sala[] salas) {
		this.salas = salas;
	}
}