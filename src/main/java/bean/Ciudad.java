package bean;

import java.io.Serializable;


/**
 * The persistent class for the ciudad database table.
 * 
 */
public class Ciudad implements Serializable {
	private static final long serialVersionUID = 1L;

	private int idCiudad;

	private String nombre;

	private Complejo[] complejos;

	public Ciudad() {
	}

	public int getIdCiudad() {
		return this.idCiudad;
	}

	public void setIdCiudad(int idCiudad) {
		this.idCiudad = idCiudad;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Complejo[] getComplejos() {
		return this.complejos;
	}

	public void setComplejos(Complejo[] complejos) {
		this.complejos = complejos;
	}

}