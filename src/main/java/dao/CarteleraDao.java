package dao;

public interface CarteleraDao {
	String listar(int complejo, String fecha);
	String	 buscar(int id);
}
