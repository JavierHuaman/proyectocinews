package dao;


public interface ClienteDao {

	String logear(String correo, String pass);
	String buscar(String dni);
	String registrar(String jsonCliente);
	String actualizar(String jsonClinte);
}
