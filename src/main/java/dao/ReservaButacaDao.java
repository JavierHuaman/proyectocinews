package dao;

public interface ReservaButacaDao {

	String verificaReservaButacas(String fecha, int idSala, String hora);
}
