package dao;

public interface ReservaDao{

	String buscar(int id);	
	String registrar(String jsonReserva);
	String buscarPorCliente(int idCliente);
	
}
