package daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

import bean.Complejo;
import dao.ComplejoDao;
import util.Conexion;

public class ComplejoDaoImpl implements ComplejoDao {

	@Override
	public String listarPorCiudad(int ciudad) {
		List<Complejo> lista = new ArrayList<Complejo>();
		Connection conexion = null;
		try {
			conexion = (new Conexion()).conectar();
			lista = new ArrayList<Complejo>();
			PreparedStatement sts = conexion.prepareStatement("select * from Complejo where idCiudad = ?");
			sts.setInt(1, ciudad);
			ResultSet rs = sts.executeQuery();
			while (rs.next()) {
				Complejo c = new Complejo();
				c.setIdComplejo(rs.getInt("idComplejo"));
				c.setNombre(rs.getString("nombre"));
				lista.add(c);
			}
			rs.close();
			conexion.close();
		} catch (Exception e) {		
			e.printStackTrace();
		}
		return new Gson().toJson(lista);
	}

}
