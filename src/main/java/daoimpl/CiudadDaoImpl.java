package daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

import bean.Ciudad;
import dao.CiudadDao;
import util.Conexion;

public class CiudadDaoImpl implements CiudadDao {

	@Override
	public String listar() {
		List<Ciudad> lista = new ArrayList<Ciudad>();
		Connection conexion = null;
		try {
			conexion = (new Conexion()).conectar();
			lista = new ArrayList<Ciudad>();
			PreparedStatement sts = conexion.prepareStatement("select * from Ciudad");
			ResultSet rs = sts.executeQuery();
			while (rs.next()) {
				Ciudad c = new Ciudad();
				c.setIdCiudad(rs.getInt("idCiudad"));
				c.setNombre(rs.getString("nombre"));
				lista.add(c);
			}
			rs.close();
			conexion.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new Gson().toJson(lista);
	}

}
