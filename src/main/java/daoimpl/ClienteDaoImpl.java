package daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.Gson;

import util.Conexion;
import util.Convert;
import bean.Cliente;
import dao.ClienteDao;

public class ClienteDaoImpl implements ClienteDao {

	@Override
	public String logear(String correo, String pass) {
		Cliente cliente = null;
		Connection conexion = null;
		try {
			conexion = (new Conexion()).conectar();
			PreparedStatement sts = conexion
					.prepareStatement("select c.* from Cliente c where c.email = ? and c.password = ?");
			sts.setString(1, correo);
			sts.setString(2, Convert.toMD5(pass));
			ResultSet rs = sts.executeQuery();
			if (rs.next()) {
				cliente = new Cliente();
				cliente.setIdCliente(rs.getInt("idCliente"));
				cliente.setApellidoMaterno(rs.getString("apellidoPaterno"));
				cliente.setApellidoPaterno(rs.getString("apellidoMaterno"));
				cliente.setDni(rs.getString("dni"));
				cliente.setEmail(rs.getString("email"));
				cliente.setCelular(rs.getString("celular"));
				cliente.setNombres(rs.getString("nombres"));
			}
			rs.close();
			conexion.close();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return new Gson().toJson(cliente);
	}

	@Override
	public String buscar(String dni) {
		Cliente cliente = null;
		Connection conexion = null;
		try {
			conexion = (new Conexion()).conectar();
			PreparedStatement sts = conexion.prepareStatement("select c.* from Cliente c where c.dni = ?");
			sts.setString(1, dni);
			ResultSet rs = sts.executeQuery();
			if (rs.next()) {
				cliente = new Cliente();
				cliente.setIdCliente(rs.getInt("idCliente"));
				cliente.setApellidoMaterno(rs.getString("apellidoPaterno"));
				cliente.setApellidoPaterno(rs.getString("apellidoMaterno"));
				cliente.setDni(rs.getString("dni"));
				cliente.setEmail(rs.getString("email"));
				cliente.setCelular(rs.getString("celular"));
				cliente.setNombres(rs.getString("nombres"));
			}
			rs.close();
			conexion.close();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return new Gson().toJson(cliente);
	}

	@Override
	public String registrar(String jsonCliente) {
		// {"dni":"41867773","nombres":"Carlos","apellidoPaterno":"Alfaro","apellidoMaterno":"Bustamante","genero":"M","fechaNacimiento":"1982-10-25","email":"calfaro@gmail.com","celular":"933344976",
		// "password":"123"}

		int r = 0;
		Cliente cliente = null;
		Connection conexion = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date hoy = new Date();
		try {
			cliente = new Gson().fromJson(jsonCliente, Cliente.class);
			conexion = (new Conexion()).conectar();
			PreparedStatement sts = conexion.prepareStatement("insert into Cliente values(null,?,?,?,?,?,?,?,?,?,?)");
			sts.setString(1, cliente.getDni());
			sts.setString(2, cliente.getNombres());
			sts.setString(3, cliente.getApellidoPaterno());
			sts.setString(4, cliente.getApellidoMaterno());
			sts.setString(5, cliente.getGenero());
			sts.setString(6, cliente.getFechaNacimiento());
			sts.setString(7, cliente.getEmail());
			sts.setString(8, cliente.getCelular());
			sts.setString(9, sdf.format(hoy));
			sts.setString(10, Convert.toMD5(cliente.getPassword()));

			r = sts.executeUpdate();
			conexion.close();
		} catch (Exception e) {
			r = 0;
			e.printStackTrace();
		}
		return new Gson().toJson(r);
	}

	public String actualizar(String jsonCliente) {
		// {"dni":"41867773","nombres":"Carlos","apellidoPaterno":"Alfaro","apellidoMaterno":"Bustamante","genero":"M","fechaNacimiento":"1982-10-25","email":"calfaro@gmail.com","celular":"933344976",
		// "password":"123","idCliente":3}

		int r = 0;
		Cliente cliente = null;
		Connection conexion = null;
		try {
			cliente = new Gson().fromJson(jsonCliente, Cliente.class);
			conexion = (new Conexion()).conectar();
			PreparedStatement sts = conexion.prepareStatement("update Cliente set dni=?, nombres=?, "
					+ "apellidoPaterno=?, apellidoMaterno=?, genero=?, fechaNacimiento=?,"
					+ "email=?,celular=?,password=? where idCliente=?");
			sts.setString(1, cliente.getDni());
			sts.setString(2, cliente.getNombres());
			sts.setString(3, cliente.getApellidoPaterno());
			sts.setString(4, cliente.getApellidoMaterno());
			sts.setString(5, cliente.getGenero());
			sts.setString(6, cliente.getFechaNacimiento());
			sts.setString(7, cliente.getEmail());
			sts.setString(8, cliente.getCelular());
			sts.setString(9, Convert.toMD5(cliente.getPassword()));
			sts.setInt(10, cliente.getIdCliente());
			r = sts.executeUpdate();
			conexion.close();
		} catch (Exception e) {
			r = 0;
			e.printStackTrace();
		}
		return new Gson().toJson(r);
	}

}
