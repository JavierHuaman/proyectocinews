package daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

import util.Conexion;
import util.Convert;
import bean.Cartelera;
import bean.Pelicula;
import dao.CarteleraDao;

public class CarteleraDaoImpl implements CarteleraDao {

	@Override
	public String listar(int complejo, String fecha) {
		List<Pelicula> lista = null;
		Connection conexion = null;
		try {
			lista = new ArrayList<Pelicula>();
			conexion = (new Conexion()).conectar();
			PreparedStatement sts = conexion.prepareStatement(
					"select * from Pelicula where idPelicula in "
					+ "(select c.idPelicula from Cartelera c join Sala s on c.idSala=s.idSala "
					+ "where s.idComplejo = ? and c.fecha = ?)");			
			sts.setInt(1, complejo);
			sts.setString(2, fecha);
			ResultSet rs = sts.executeQuery();
			while (rs.next()) {
				Pelicula p = Convert.obtenerPelicula(rs, conexion);
				ArrayList<Cartelera> carteleras = null;
				try {
					carteleras = new ArrayList<Cartelera>();
					ResultSet carteleraResult = conexion
							.prepareStatement("select * from Cartelera where idPelicula = " + p.getIdPelicula())
							.executeQuery();
					while (carteleraResult.next()) {
						Cartelera c = new Cartelera();
						c.setHoraInicio(carteleraResult.getString("horaInicio"));
						c.setIdCartelera(carteleraResult.getInt("idCartelera"));
						c.setIdSala(carteleraResult.getInt("idSala"));
						carteleras.add(c);
					}
					Cartelera[] cartArr = new Cartelera[carteleras.size()];
					p.setCarteleras(carteleras.toArray(cartArr));
					carteleraResult.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				lista.add(p);
			}
			rs.close();
			conexion.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new Gson().toJson(lista);
	}

	@Override
	public String buscar(int id) {
		Connection conexion = null;
		Cartelera cartelera = null;
		try {
			conexion = (new Conexion()).conectar();
			PreparedStatement sts = conexion.prepareStatement("select * from Cartelera where idCartelera = ?");
			sts.setInt(1, id);
			ResultSet rs = sts.executeQuery();
			if (rs.next()) {
				cartelera = Convert.obtenerCartelera(rs, conexion);
			}
			rs.close();
			conexion.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new Gson().toJson(cartelera);
	}
	
}
