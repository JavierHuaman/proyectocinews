package daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import util.Conexion;
import util.Convert;
import bean.Cartelera;
import bean.Cliente;
import bean.Reserva;
import bean.Reservabutaca;

import com.google.gson.Gson;

import dao.ReservaDao;

public class ReservaDaoImpl implements ReservaDao {

	@Override
	public String buscar(int id) {
		Reserva bean = null;
		Connection conexion = null;
		try {

			conexion = (new Conexion()).conectar();
			PreparedStatement sts = conexion.prepareStatement("select * from Reserva where idReserva = ?");
			sts.setInt(1, id);
			ResultSet rs = sts.executeQuery();
			if (rs.next()) {
				bean = new Reserva();
				bean.setIdReserva(rs.getInt("idReserva"));
				bean.setEstado(rs.getString("estado"));
				bean.setFechaReserva(rs.getString("fechaReserva"));
				bean.setNumeroTicket(rs.getString("numeroTicket"));
				/********************************/
				Cliente cli = null;
				try {
					PreparedStatement clis = conexion
							.prepareStatement("select * from Cliente where idCliente = " + rs.getInt("idCliente"));
					ResultSet sr = clis.executeQuery();
					if (sr.next()) {
						cli = new Cliente();
						cli.setIdCliente(sr.getInt("idCliente"));
						cli.setNombres(sr.getString("nombres"));
						cli.setApellidoMaterno(sr.getString("apellidoMaterno"));
						cli.setApellidoPaterno(sr.getString("apellidoPaterno"));
						cli.setDni(sr.getString("dni"));
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
				bean.setCliente(cli);
				/********************************/
				PreparedStatement carts = conexion
						.prepareStatement("select * from Cartelera where idCartelera = " + rs.getInt("idCartelera"));
				ResultSet carr = carts.executeQuery();
				Cartelera cart = null;
				if (carr.next()) {
					cart = Convert.obtenerCartelera(carr, conexion);
				}
				bean.setCartelera(cart);
				carr.close();
				/********************************/
				ArrayList<Reservabutaca> butacas = null;
				try {
					ResultSet resultbutacas = conexion
							.prepareStatement("select * from ReservaButaca where idReserva = " + rs.getInt("idReserva"))
							.executeQuery();
					butacas = new ArrayList<Reservabutaca>();
					while (resultbutacas.next()) {
						Reservabutaca rb = new Reservabutaca();
						rb.setButaca(resultbutacas.getString("butaca"));
						rb.setPrecio(resultbutacas.getDouble("precio"));
					
						butacas.add(rb);
					}
					resultbutacas.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				Reservabutaca[] rbArr = new Reservabutaca[butacas.size()];
				bean.setReservabutacas(butacas.toArray(rbArr));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new Gson().toJson(bean);
	}

	@Override
	public String registrar(String jsonReserva) {
		// Con lista de butacas
		// {"idCartelera":1,"fechaReserva":"2015-11-28","idCliente":1,"estado":"Vigente",
		// "reservabutacas":[{"butaca":"A4","precio":12.0,"idSala":1},
		// {"butaca":"A5","precio":12.0,"idSala":1}]}

		// Sin lista de butacas
		// {"idCartelera":1,"fechaReserva":"2015-11-28","idCliente":1,"estado":"Vigente",
		// "reservabutacas":[]}
		Connection conexion = null;
		Reserva reserva;
		try {
			reserva = new Gson().fromJson(jsonReserva, Reserva.class);
			reserva.setNumeroTicket(UUID.randomUUID().toString().substring(0, 8).toUpperCase());

			conexion = (new Conexion()).conectar();
			PreparedStatement sts = conexion.prepareStatement("insert into Reserva values (null, ?, ?, ?, ?, ?)",
					Statement.RETURN_GENERATED_KEYS);
			sts.setInt(1, reserva.getIdCartelera());
			sts.setString(2, reserva.getFechaReserva());
			sts.setInt(3, reserva.getIdCliente());
			sts.setString(4, reserva.getEstado());
			sts.setString(5, reserva.getNumeroTicket());
			int r = sts.executeUpdate();
			if (r > 0) {
				if (reserva.getReservabutacas() != null && reserva.getReservabutacas().length != 0) {
					try {
						ResultSet generatedKeys = sts.getGeneratedKeys();
						if (generatedKeys.next()) {
							reserva.setIdReserva((int) generatedKeys.getLong(1));
							for (Reservabutaca rb : reserva.getReservabutacas()) {
								PreparedStatement stament = conexion
										.prepareStatement("insert into ReservaButaca values (null, ?, ?, ?, ?)");
								stament.setInt(1, reserva.getIdReserva());
								stament.setString(2, rb.getButaca());
								stament.setInt(3, rb.getIdSala());
								stament.setDouble(4, rb.getPrecio());
								r = stament.executeUpdate();
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
						return null;
					}
				}
			}
			sts.close();
			conexion.close();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return new Gson().toJson(reserva);
	}

	@Override
	public String buscarPorCliente(int idCliente) {
		Reserva bean = null;
		Connection conexion = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date hoy = new Date();
		List<Reserva> lista = new ArrayList<Reserva>();
		try {

			conexion = (new Conexion()).conectar();
			PreparedStatement sts = conexion
					.prepareStatement("select * from Reserva where idCliente = ? and fechaReserva>=?");
			sts.setInt(1, idCliente);
			sts.setString(2, sdf.format(hoy));

			ResultSet rs = sts.executeQuery();
			while (rs.next()) {
				bean = new Reserva();
				bean.setIdReserva(rs.getInt("idReserva"));
				bean.setEstado(rs.getString("estado"));
				bean.setFechaReserva(rs.getString("fechaReserva"));
				bean.setNumeroTicket(rs.getString("numeroTicket"));
				lista.add(bean);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new Gson().toJson(lista);
	}

}
