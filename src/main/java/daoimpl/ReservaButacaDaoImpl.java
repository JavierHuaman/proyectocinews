package daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import util.Conexion;

import com.google.gson.Gson;

import dao.ReservaButacaDao;

public class ReservaButacaDaoImpl implements ReservaButacaDao {

	@Override
	public String verificaReservaButacas(String fecha, int idSala, String hora) {
		Connection conexion = null;
		List<String> lista = null;

		try {
			conexion = (new Conexion()).conectar();
			lista = new ArrayList<String>();
			PreparedStatement sts = conexion
					.prepareStatement("select r.* from ReservaButaca r join Reserva v on r.idReserva = v.idReserva "
							+ " join Cartelera c on v.idCartelera=c.idCartelera where v.fechaReserva = ? "
							+ "and r.idSala = ? and c.horaInicio = ?");
			sts.setString(1, fecha);
			sts.setInt(2, idSala);
			sts.setString(3, hora);
			ResultSet rs = sts.executeQuery();
			while (rs.next()) {
				lista.add(rs.getString("butaca"));
			}
			rs.close();
			conexion.close();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return new Gson().toJson(lista);
	}

}
